# Use this file to share variables across different threads/files/cogs

def init():
    global voice_connections, config
    voice_connections = {}
    config = {}
