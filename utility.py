from discord import Embed, Colour


# syntax for commands
def syntax(*txt):
    def decorator(func):
        func.syntax = txt
        return func
    return decorator


class EmbedType:
    DEFAULT = 0
    WARN = 1
    INFO = 2
    ERROR = 4


def CreateEmbed(embed_type: EmbedType = EmbedType.DEFAULT, title: str = None, description: str = None):
    color = None
    embed_title = ""
    if embed_type == 0:
        embed_title = "Antwort"
        color = Colour.blurple()
    if embed_type == 1:
        embed_title = "Warnung"
        color = Colour.yellow()
    if embed_type == 2:
        embed_title = "Info"
        color = Colour.orange()
    if embed_type == 4:
        embed_title = "Fehler"
        color = Colour.red()

    if title:
        embed_title = title

    return Embed(title=embed_title, description=description, color=color)
