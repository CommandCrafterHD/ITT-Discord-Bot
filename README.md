
<html>
    <head>
        <title></title>
        <meta charset='utf-8'/>
        <link rel="stylesheet" type="text/css" href="./README.html_files/github-markdown.css">
<!--         <style>
            markdown-body {
                box-sizing: border-box;
                min-width: 200px;
                max-width: 980px;
                margin: 0 auto;
                padding: 45px;
            }
        </style> -->
        <script>
            window.onload = function() {
                if (document.querySelector("script[type=\"math/tex; mode=display\"]") !== null) {
                    var mathjax = document.createElement("script");
                    mathjax.src = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML";
                    document.head.appendChild(mathjax);
                }
                if (document.getElementsByTagName("code").length !== 0) {
                    var highlight = document.createElement("script");
                    var highlightcss = document.createElement("link");
                    highlight.src = "http://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/highlight.min.js";
                    highlightcss.rel = "stylesheet";
                    highlightcss.href = "http://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/github.min.css";
                    highlight.onload = function() {hljs.initHighlighting();};
                    document.head.appendChild(highlight);
                    document.head.appendChild(highlightcss);
                }
            }
        </script>
    </head>
    <body>
        <article class="markdown-body">
            <h1 id="itt-discord-bot">ITT Discord Bot</h1>
<h2 id="installation">Installation</h2>
<p><strong>Prerequisities</strong><br></p>
<ul>
<li><a href="https://discordpy.readthedocs.io/en/stable/#">discord.py</a></li><li><a href="https://pyyaml.org/wiki/PyYAMLDocumentation">pyyaml</a></li><li><a href="https://pypi.org/project/beautifulsoup4/">bs4</a></li><li><a href="https://pypi.org/project/selenium/">selenium</a></li><li><a href="https://pypi.org/project/geckolib/">geckolib</a></li><li><a href="https://github.com/mozilla/geckodriver/releases">geckodriver (if installing geckolib with pip)</a></li></ul>
<hr>
<p><strong>1. Clone the Repository</strong><br>
Clone the repository onto the machine on which you want to host the bot.</p>
<hr>
<p><strong>2. Enter your Discord Bot Token</strong><br>
Enter your Discord Bot Token into
<code>config/EXAMPLE_config.yaml</code><br>
Information on how to create the Discord Bot Application and where to find your Discord Bot Token can be found <a href="https://docs.discordbotstudio.org/setting-up-dbs/finding-your-bot-token">here</a></p>
<hr>
<p><strong>3. Start the bot</strong><br>
Start the bot by running following command:<br>
<code>$ python main.py</code></p>
<hr>
<!--         </article>
    </body> -->
</html>
