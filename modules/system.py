from discord import Embed, Color
from discord.ext import commands
from discord.ext.commands import Cog
from discord.ext.commands import MissingPermissions, CommandNotFound, MissingRequiredArgument, BadArgument


class SystemCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @Cog.listener()
    async def on_command_error(self, ctx: commands.Context, exc: BaseException):
        if isinstance(exc, MissingPermissions):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description="Du brauchst admin rechte für diesen Befehl!",
                    color=Color.red()))
        elif isinstance(exc, MissingRequiredArgument):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description=f"Der Befehl den du verwenden möchtest braucht mehr/andere Argumente, versuch mal \n\n**{ctx.prefix}help {ctx.command}**\n\nfür mehr Infos über den command!",
                    color=Color.red()))
        elif isinstance(exc, BadArgument):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description="Scheinbar gab es einen Formatierungsfehler in deiner Nachricht! Falls du einen String mit mehr als einem Wort senden wolltest, stell sicher das nur am anfang und ende Anführungszeichen sind und jegliche Anführungszeichen IM string mit einem \ escaped sind!",
                    color=Color.red()))
        elif isinstance(exc, CommandNotFound):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description=str(exc.args[0]),
                    color=Color.red()))
        else:
            raise exc


async def setup(bot: commands.Bot):
    await bot.add_cog(SystemCog(bot))
