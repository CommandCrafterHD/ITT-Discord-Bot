from discord.ext import commands
from discord.ext.commands import Cog


class AutoroleCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @Cog.listener()
    async def on_member_join(self, member):
        for role in member.roles:
            if role.name == "Member":
                await member.add_roles(role)
                print(f'{member.name} was given {role.name}')


async def setup(bot: commands.Bot):
    await bot.add_cog(AutoroleCog(bot))
