import xmltodict
from files import globals
from utility import syntax, CreateEmbed, EmbedType
from discord import File
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TimetableCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    async def FetchTimetable(self):
        options = Options()
        options.headless = True
        browser = webdriver.Firefox(options=options)
        browser.get('https://mese.webuntis.com/WebUntis/?school=BK-Technik-Siegen#/basic/timetable')

        # Wait until the input is there
        elem = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.TAG_NAME, "input"))  # This is a dummy element
        )

        # Enter our class
        element_enter = browser.find_element(By.TAG_NAME, "input")
        element_enter.send_keys("ITT322")
        element_enter.send_keys(Keys.ENTER)

        elem = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.CLASS_NAME, "timetableContent"))
        )

        if elem.is_enabled():
            table = browser.find_element(By.CLASS_NAME, 'un-timetable-page__body')
            table.screenshot('./files/test.png')

        browser.quit()
        return True

    @syntax()
    @commands.command()
    async def timetable(self, ctx: CommandContext):
        em = CreateEmbed(EmbedType.INFO, "Lade...", "Neuer Stundenplan wird gefetcht, stand by...")
        msg = await ctx.send(embed=em)
        returned = await self.FetchTimetable()
        if returned:
            em = CreateEmbed(EmbedType.DEFAULT, "Stundenplan")
            file = File("files/test.png", filename="image.png")
            em.set_image(url="attachment://image.png")
            await msg.edit(embed=em, attachments=[file])


async def setup(bot: commands.Bot):
    await bot.add_cog(TimetableCog(bot))
