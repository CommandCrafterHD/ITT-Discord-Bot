from discord import Embed, Color, opus, PCMVolumeTransformer, FFmpegPCMAudio
from discord.ext import commands
from discord.ext.commands import Cog, Context as CommandContext
from utility import syntax, EmbedType, CreateEmbed
from files import globals
import youtube_dl
import asyncio

if not opus.is_loaded():
    print("ERROR! Opus could not be loaded!")

youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class MusicCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @syntax('')
    @commands.command()
    async def join(self, ctx: CommandContext):
        if ctx.author.voice.channel:
            channel = await ctx.author.voice.channel.connect()
            globals.voice_connections[ctx.guild.id] = channel

    @syntax('')
    @commands.command()
    async def leave(self, ctx: CommandContext):
        conn = globals.voice_connections[ctx.guild.id]
        if conn:
            if conn.is_connected():
                await globals.voice_connections[ctx.guild.id].disconnect()
            else:
                pass  # TODO: THROW ERROR HERE
        else:
            pass  # TODO: THROW ERROR HERE

    @syntax('<Youtube Link>')
    @commands.command()
    async def play(self, ctx: CommandContext, url: str = None):
        if url:
            try:
                connection = globals.voice_connections[ctx.guild.id]
            except:
                connection = None
            if not connection:
                print('Test')
                # TODO: THROW ERROR HERE
                return
            else:
                player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
                if connection.is_playing():
                    connection.stop()
                connection.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
                em = CreateEmbed(embed_type=EmbedType.DEFAULT, title="Started Playing", description=f"Spiele jetzt: **{player.title}**!")
                await ctx.send(embed=em)




async def setup(bot: commands.Bot):
    await bot.add_cog(MusicCog(bot))
