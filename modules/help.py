from utility import syntax
from discord import Embed, Colour, __version__
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from sys import version


class HelpCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @syntax('')
    @commands.command()
    async def about(self, ctx: CommandContext):
        em = Embed(
            title="ITT Bot",
            description="",
            color=Colour.blurple()
        )
        em.set_thumbnail(url=ctx.bot.user.avatar.url)
        em.add_field(name="Generelle Infos", value=f"Python Version: **{version}**\n"
                                                       f"Discord.py Version: **{__version__}**\n"
                                                       f"Ping: **{str(ctx.bot.latency)[:4]} Seconds**\n"
                                                       f"Prefix: **{ctx.prefix}**", inline=False)
        em.add_field(name="Maintainer", value="[Flo](https://cegledi.net/) [xqtc](https://xqtc.online) [Marie](https://marie.software)")
        em.add_field(name="Brauchst du mehr infos?", value=f"Für alle Befehle, versuch **{ctx.prefix}help**", inline=False)
        em.add_field(name="Wichtige links",
                     value="[GitLab](https://gitlab.com/CommandCrafterHD/ITT-Discord-Bot)", inline=False)
        await ctx.send(embed=em)

    @syntax("<command>")
    @commands.command()
    async def help(self, ctx: CommandContext, cmd: str = None):
        txt = f"```md\n### Hier ist eine Liste aller Module und ihrer Befehle! ###\nNutze {ctx.prefix}help <Befehl> um mehr Infos über einen Befehl zu erhalten!```\n```diff\n"
        cog_list = []
        if not cmd:
            for cog in self.bot.cogs:
                name = cog
                if len(self.bot.get_cog(cog).get_commands()) == 0:
                    continue
                if name.lower().endswith("cog"):
                    name = name[:-3]
                cmds = [name]
                for cmd in self.bot.get_cog(cog).get_commands():
                    cmds.append(ctx.prefix + str(cmd.name))
                cog_list.append(cmds)
            cog_list.sort(key=len)
            for i in cog_list:
                txt += f"\n- {i[0]}\n"
                for a in i[1:]:
                    txt += f"   {a}\n"
            txt += "```"
            await ctx.message.channel.send(txt)
        else:
            if self.bot.get_command(cmd):
                command = self.bot.get_command(cmd)
                cmd_syntax = ""
                if hasattr(command.callback, 'syntax'):
                    for i in command.callback.syntax:
                        cmd_syntax += f"**{ctx.prefix}{cmd.lower()} {i}**\n"
                else:
                    cmd_syntax = f"**{ctx.prefix}{cmd.lower()}**"
                em = Embed(
                    color=Colour.blurple(),
                    title="Help",
                    description="")
                em.add_field(name="Befehl", value=cmd.title(), inline=False)
                if command.description:
                    em.add_field(name="Beschreibung", value=command.description, inline=False)
                em.add_field(name="Syntax", value=cmd_syntax, inline=False)
                await ctx.send(embed=em)
            else:
                await ctx.send(
                    embed=Embed(
                        title="Error",
                        description=f"Der Befehl **{cmd.lower()}** konnte nicht gefunden werden! Nutz **{ctx.prefix}help** für eine Liste aller befehle!",
                        color=Colour.red()))


async def setup(bot: commands.Bot):
    await bot.add_cog(HelpCog(bot))
