import yaml
from os import path, chdir, listdir
from shutil import copyfile
from files import colors, globals
from discord import Intents
from discord.ext import commands
from time import time

realPath = path.realpath(__file__)
name_length = len(path.basename(__file__))
chdir(realPath[:-name_length])
timer = None

# Initializing global variables
globals.init()

# Loading the config
try:
    file = open("config/config.yaml")
    globals.config = yaml.load(file.read(), Loader=yaml.SafeLoader)
except FileNotFoundError:  # And creating a config.yaml file if not present yet
    try:
        print(f"{colors.YELLOW}SETTINGS FILE MISSING. Creating one...{colors.END}")
        copyfile("config/EXAMPLE_config.yaml", "config/config.yaml")
        print(f"{colors.GREEN}Settings file created! Please edit it and restart the bot.{colors.END}")
        exit(0)
    except PermissionError:
        print(f"{colors.RED}COULD NOT CREATE SETTINGS FILE. MISSING PERMISSION!{colors.END}")
        exit(1)

# Setting up our bot-client
intents = Intents.default()
intents.message_content = True
client: commands.bot = commands.AutoShardedBot(command_prefix=globals.config["prefix"], intents=intents)


async def load_modules():
    # Finding all modules
    modules = []
    for i in listdir("modules"):
        if i.endswith(".py"):
            modules.append("modules." + i[:-3])

    # Removing the integrated help command
    client.remove_command('help')

    print(f"{colors.YELLOW} LOADING MODULES!{colors.END}")
    print("--" * 20)

    # Loading all found modules
    for module in modules:
        try:
            await client.load_extension(module)
            print(f"{colors.CYAN}Module {module}{colors.END}: {colors.GREEN}LOADED{colors.END}")
        except Exception as e:
            print(f"{colors.CYAN}Module {module}{colors.END}: {colors.RED}NOT LOADED\n({e}){colors.END}")

    print("--" * 20)


@client.listen('on_ready')
async def on_ready():
    print(f"{colors.YELLOW} BOT STARTED!{colors.END}")
    print("--" * 20)
    print(f"{colors.CYAN}Name{colors.END}: {colors.YELLOW}{client.user.name}{colors.END}")
    print(f"{colors.CYAN}ID{colors.END}: {colors.YELLOW}{client.user.id}{colors.END}")
    print(f"{colors.CYAN}Prefix{colors.END}: {colors.YELLOW}{client.command_prefix}{colors.END}")
    print(f"{colors.CYAN}Launched in: {colors.YELLOW}{time() - timer} seconds!{colors.END}")
    print("--" * 20)
    await load_modules()

# Finally running the bot!
if __name__ == "__main__":
    print("--" * 20)
    print(f"{colors.GREEN} BOT STARTING! {colors.END}")
    print("--" * 20)
    timer = time()
    if globals.config["disable_logs"]:
        client.run(globals.config["token"], log_handler=None)
    else:
        client.run(globals.config["token"])
