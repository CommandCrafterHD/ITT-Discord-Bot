@echo off
color 0E
title ITT-Discord-Bot
:start
python main.py
title RESTARTING BOT IN 3 SECONDS
timeout /t 1 /nobreak > nul
title RESTARTING BOT IN 2 SECONDS
timeout /t 1 /nobreak > nul
title RESTARTING BOT IN 1 SECOND
timeout /t 1 /nobreak > nul
goto start